FROM python:3.8

RUN mkdir /code
WORKDIR /code
ADD . /code

RUN chmod +x entrypoint.sh; pip install -r requirements.txt


ENTRYPOINT ["/code/entrypoint.sh"]
