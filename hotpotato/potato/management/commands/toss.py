from django.core.management.base import BaseCommand, CommandError
from potato.models import Potato

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        Potato.objects.create()
        self.stdout.write(self.style.SUCCESS('Passing the Potato'))