#!/bin/bash

cd /code/hotpotato

python manage.py makemigrations
python manage.py migrate
# pass the potato
python manage.py toss

python manage.py runserver 0.0.0.0:8000
